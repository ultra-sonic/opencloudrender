import re
from opencloudrender.parsers import RenderScene

import os,logging

class vrscene(RenderScene):
    def __init__(self , ifd_path ):
        super(vrscene, self).__init__( ifd_path )
        self.asset_patterns=[' file=".*"' , 'ray_loadotl ".+?"']
        self.include_patterns=['#include.*vrscene'] # files that need additional parsing

        # ray_image "-f" "OpenEXR" "/foo/bar/foobar.beauty.00076.exr"
        # ray_property plane planefile "/foo/bar/foobar.direct_emission.00076.exr"
        # paths MUST start with "/"
        self.image_patterns=[ ' img_file=".*"' ] #
        self.assets=[ ifd_path ]
        self.images=[]

    def get_output_images_list(self):
        frame_number_match=re.compile( '_[0-9][0-9][0-9][0-9]*\.vrscene' ).search( self.scene_path )
        frame_number=self.scene_path[ frame_number_match.start()+1 : frame_number_match.end()-8 ]
        #print "?????????????????????????????????????????????????????"
        #print frame_number
        #print frame_number_match.start()
        #print frame_number_match.end()
        #print "?????????????????????????????????????????????????????"

        img_dir=""
        tmp_imges=[]
        with open( self.scene_path , 'r' ) as scene:
            for line in scene:
                for pattern in [' img_dir=".*";']:
                    regex = re.compile( pattern )
                    match = regex.search( line )
                    if match != None:
                        img_dir= line[ match.start() + pattern.find('"') : match.end()-1  ]
                        print "DEBUG img_dir: " + img_dir
                for pattern in self.image_patterns:
                    regex = re.compile( pattern )
                    match = regex.search( line )
                    if match != None:
                        image_plane = line[ match.start() + pattern.find('"') : match.end()-1  ]
                        if image_plane not in tmp_imges:
                            tmp_imges.append( image_plane )
        for img in tmp_imges:
            img_incl_framenum=img.strip('"')[:-4] + frame_number + img.strip('"')[-4:]
            image_plane=os.path.join( img_dir.strip('"') , img_incl_framenum )
            self.images.append( image_plane )
            print "DEBUG image_planes: " + image_plane

    def get_scene_dependencies( self , recursion_depth=0 ):
        #ray_procedural file file "/NAS/001_PROJEKTE/22_CREED/04_Production/_ASSETS_/Character/Crowd_Cache/v001/OUT_Cache_P1_v001.0001.bgeo"
        logging.debug('get_scene_dependencies: ' + self.scene_path )
        logging.debug('recursion_depth: ' + str( recursion_depth ) )

        included_scenes = []
        with open( self.scene_path , 'r' ) as scene:
            for line in scene:
                for pattern in self.include_patterns:
                    regex = re.compile( pattern )
                    match = regex.search( line )
                    if match != None:
                        included_scene = line[ line.find('"/') + 1 : match.end()-1  ]
                        included_scenes.append( included_scene )
                        #print "DEBUG included_scene: " + included_scene

                for pattern in self.asset_patterns:
                    matches = re.findall( pattern , line )
                    for match in matches:
                        file_path = match[ match.find('"') + 1 : -1  ]
                        if file_path != '' and file_path.startswith('/'):
                            #if os.path.isfile( file_path ): needed removal for reverse sync
                            if file_path not in self.assets:
                                self.assets.append( file_path )
                                #if file_path.find('mainstand_add_up_dirt_01')>-1:
                                #print "DEBUG asset path: " + file_path


        #recurse into included bgeos
        for included_scene in included_scenes:
            # check for relative include paths if file does not exist
            # assets.extend( self.get_scene_dependencies( included_scene , recursion_depth=recursion_depth+1) )
            pass # add bgeo parsing here

        self.assets.extend( included_scenes )
        logging.debug( self.assets )
        #return assets

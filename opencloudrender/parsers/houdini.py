import re
from opencloudrender.parsers import RenderScene

import os,logging

"""
obsolete mantra ifd filters

import mantra
#def filterGeometry():
def filterInstance():
	name = mantra.property('object:name')
	print name
	procedural = mantra.property('object:*')
	print procedural
	#os._exit(0)


def filterRender():
	print ">>>>>>>>>>>>>>>>"
	print ">>>>>>>> EXITING!"
	print ">>>>>>>>>>>>>>>>"
	os._exit(0)
"""

class IFD(RenderScene):
	def __init__(self , ifd_path ):
		super(IFD, self).__init__( ifd_path )
		self.asset_patterns=['map.? ".+?"' , 'map.? = ".+?"' , 'ray_loadotl ".+?"']
		self.include_patterns=['ray_procedural file file ".+?"'] # files that need additional parsing

		# ray_image "-f" "OpenEXR" "/foo/bar/foobar.beauty.00076.exr"
		# ray_property plane planefile "/foo/bar/foobar.direct_emission.00076.exr"
		# paths MUST start with "/"
		self.image_patterns=[ 'ray_image.+?"/.+?"' , 'ray_property plane planefile "/.+?"' ]
		self.assets=[ ifd_path ]
		self.images=[]

	def get_output_images_list(self):
		# ray_image "/NAS/001_PROJEKTE/22_CREED/04_Production/_ASSETS_/Environment/StadiumModel/3D_Renders/Crowd/GPF_202_3710_v001/Crowd.beauty.00148.exr"
		# ray_property plane planefile "/NAS/001_PROJEKTE/22_CREED/04_Production/_ASSETS_/Environment/StadiumModel/3D_Renders/Crowd/GPF_202_3710_v001/Crowd.direct_comp.00148.exr"
		self.get_scene_dependencies()

	def get_scene_dependencies( self , recursion_depth=0 ):
		#ray_procedural file file "/NAS/001_PROJEKTE/22_CREED/04_Production/_ASSETS_/Character/Crowd_Cache/v001/OUT_Cache_P1_v001.0001.bgeo"
		logging.debug('get_scene_dependencies: ' + self.scene_path )
		logging.debug('recursion_depth: ' + str( recursion_depth ) )



		included_scenes = []
		#assets = []
		with open( self.scene_path , 'r' ) as scene:
			for line in scene:
				for pattern in self.image_patterns:
					regex = re.compile( pattern )
					match = regex.search( line )
					if match != None:
						image_plane = line[ line.find('"/') + 1 : match.end()-1  ]
						if image_plane not in self.images:
							self.images.append( image_plane )
							print "DEBUG image_planes: " + image_plane

				for pattern in self.include_patterns:
					regex = re.compile( pattern )
					match = regex.search( line )
					if match != None:
						included_scene = line[ line.find('"/') + 1 : match.end()-1  ]
						included_scenes.append( included_scene )
						print "DEBUG included_scene: " + included_scene

				for pattern in self.asset_patterns:
					matches = re.findall( pattern , line )
					for match in matches:
						file_path = match[ match.find('"') + 1 : -1  ]
						if file_path != '' and file_path.startswith('/'):
							#if os.path.isfile( file_path ): needed removal for reverse sync
							if file_path not in self.assets:
								self.assets.append( file_path )
								#if file_path.find('mainstand_add_up_dirt_01')>-1:
								print "DEBUG asset path: " + file_path


		#recurse into included bgeos
		for included_scene in included_scenes:
			# check for relative include paths if file does not exist
			# assets.extend( self.get_scene_dependencies( included_scene , recursion_depth=recursion_depth+1) )
			pass # add bgeo parsing here

		self.assets.extend( included_scenes )
		logging.debug( self.assets )
		#return assets

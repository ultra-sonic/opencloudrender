import os , sys , subprocess, renderJobSubmission , s3IO , sceneSync
from opencloudrender.vrayUtils import get_vray_settings
from pathUtils import validate_file_path, add_padding_to_image_path


from PySide import QtGui
import ui

import logging
import tempfile

# ENABLE LOGGING
log_dir = tempfile.gettempdir()
log_file = os.path.join( os.path.abspath(log_dir) , 'opencloudrender.log' )
logging.basicConfig(
    format='%(asctime)s ::: %(levelname)s ::: %(message)s',
    datefmt='%m/%d/%Y %H:%M:%S',
    level=logging.DEBUG,
    filename=log_file
)
print "logging to: \n" + log_file

data_bucket_name        = os.environ.get('DATA_BUCKET' , 'env var DATA_BUCKET not set!' )
logging.debug( "S3 Data Bucket: " + data_bucket_name )

def showUI():
    logging.debug( 'showing UI')
    app = QtGui.QApplication(sys.argv)
    mySW = ui.ControlMainWindow()

    desktop=QtGui.QDesktopWidget()
    # Position on top left of second screen for debugging
    mySW.move( desktop.screenGeometry( desktop.screenCount() -1 ).left() , 0 )
    # center on current screen for release
    #mySW.move( desktop.screenGeometry(0).center() - mySW.rect().center() )

    mySW.show()
    sys.exit(app.exec_())

def upload_image_s3( scene_path , file_path , strip_path_prefix , start_frame , end_frame , step_size ):
    """
    :rtype : None
    :param scene_path:
    :param file_path: 
    :param strip_path_prefix: 
    :param start_frame: 
    :param end_frame: 
    :param step_size: 
    """
    if scene_path.endswith('.vrscene'):
        vray_settings = get_vray_settings( scene_path )
        padding = vray_settings[ "anim_frame_padding" ]
        for frame_number in range( start_frame , end_frame+1 , step_size ):
            file_path_frame  = add_padding_to_image_path( file_path , padding ) % frame_number
            #data_bucket_name = os.environ['DATA_BUCKET']
            s3IO.upload_file(
                data_bucket_name,
                validate_file_path( file_path_frame ),
                strip_path_prefix=strip_path_prefix
            )

def upload_image_ftp( file_path , strip_path_prefix ):
    """
    :rtype : None
    :param file_path: 
    :param strip_path_prefix: 
    """
    pass

def makedirs( file_list ):
    # make all directories if they do not exist
    for file_path in file_list:
        file_dir = os.path.dirname( file_path )
        if os.path.isdir(file_dir)==False:
            print "creating dir: " + file_dir
            os.makedirs( file_dir )

def rsync( file_list , source , dest , dry_run=False ):
    cmd = [ 'rsync' , '-avz' , '-O' , '-K' , '-e' , '"ssh"' , '--from0' , '--files-from=-' , source , dest ]
    if dry_run:
        cmd.append('--dry-run')
    # p = subprocess.Popen( cmd , stdout=subprocess.PIPE , stdin=subprocess.PIPE , stderr=subprocess.STDOUT )
    p = subprocess.Popen( cmd , stdin=subprocess.PIPE )
    stdout = p.communicate( input = '\0'.join( file_list ) )[0]
    #print( stdout.decode() )


def syncRenderscene( ifd_path ):
    if ifd_path.endswith(".ifd"):
        import parsers.houdini as sceneParser
        render_scene_object = sceneParser.IFD( ifd_path )
    elif ifd_path.endswith(".vrscene"):
        import parsers.vray as sceneParser
        render_scene_object = sceneParser.vrscene( ifd_path )
    render_scene_object.get_scene_dependencies()
    rsync( render_scene_object.assets )

def syncRendersceneReverse( ifd_path , source , dest ):
    # get renderscene first!

    makedirs( [ ifd_path ] )
    rsync( [ ifd_path ] , source , dest )

    if ifd_path.endswith(".ifd"):
        import parsers.houdini as sceneParser
        render_scene_object = sceneParser.IFD( ifd_path )
    elif ifd_path.endswith(".vrscene"):
        import parsers.vray as sceneParser
        render_scene_object = sceneParser.vrscene( ifd_path )

    render_scene_object.get_scene_dependencies()
    makedirs( render_scene_object.assets )
    render_scene_object.get_output_images_list()
    makedirs( render_scene_object.images )
    rsync( render_scene_object.assets , source , dest ) # todo remove dryrun!!!

def syncRenderedImages( ifd_path , source , dest):
    if ifd_path.endswith(".ifd"):
        import parsers.houdini as sceneParser
        render_scene_object = sceneParser.IFD( ifd_path )
    elif ifd_path.endswith(".vrscene"):
        import parsers.vray as sceneParser
        render_scene_object = sceneParser.vrscene( ifd_path )
    render_scene_object.get_output_images_list()
    rsync( render_scene_object.images , source , dest , dry_run=False )



def submit_vrscene( vrscene_path ):
    """
    :rtype : None
    :param vrscene_path: 
    """
    #vraySceneSync.uploadWithDependencies( data_bucket_name , vrscene_path )


if __name__ == '__main__':
    showUI()
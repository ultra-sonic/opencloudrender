PREREQUISITES:
==============
Python - on Windows only:
----------------

Please install python 2.7.9 including pip

PySide
------

Linux - yum based distros:

```
sudo yum install python-pyside
```

> **Note:**
> On Centos 7 you have to compile & install PySide by yourself as described here:
> http://unix.stackexchange.com/questions/160496/how-to-install-pyside-package-for-centos-7

OSX:

install macports and do:
```
sudo port install py-pyside
```
Windows:
```
pip install -U PySide
```

INSTALL:
========

```
git clone https://github.com/ultra-sonic/opencloudrender.git
cd opencloudrender
#get cgru rendermanger - submodule
git submodule init
git submodule update
```
> **Note:**
> I originally intended to install opencloudrender using distutils, but due to the dependency on "cgru/afanasy" this seems impossible.
> If anybody comes up with a clever way let me know!

CONFIGURE:
==========
Afanasy
---------
you must configure the hostname or ip of your afserver here:
cgru/afanasy/config_default.json

just change this line:
"af_servername":"yourserver.here.com",
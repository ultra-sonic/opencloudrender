#MY_INSTANCE_ID=i-XXXXXXXX
VOLUME_ID=$1
DESCRIPTION=$2
# DAY=$(date +%Y-%m-%d\ %H:%M)
aws ec2 create-snapshot --volume-id ${VOLUME_ID} --description "$DESCRIPTION"

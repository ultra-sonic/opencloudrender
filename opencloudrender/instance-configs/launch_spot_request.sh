DATE=`date --utc`

aws ec2 describe-spot-price-history --instance-types c4.8xlarge m4.10xlarge --availability-zone eu-west-1a --start-time "$DATE" --product-descriptions "Linux/UNIX (Amazon VPC)"
aws ec2 describe-spot-price-history --instance-types c4.8xlarge m4.10xlarge --availability-zone eu-west-1b --start-time "$DATE" --product-descriptions "Linux/UNIX (Amazon VPC)"
aws ec2 describe-spot-price-history --instance-types c4.8xlarge m4.10xlarge --availability-zone eu-west-1c --start-time "$DATE" --product-descriptions "Linux/UNIX (Amazon VPC)"

echo -n "Instance type (1=c4.8xlarge,2=m4.10xlarge,3=c3.8xlarge): "
read instance_type_int

case "${instance_type_int}" in
        1)
            instance_type="c4.8xlarge"
            ;;
         
        2)
            instance_type="m4.10xlarge"
            ;;
        3)
            instance_type="c3.8xlarge"
            ;;
        *)
            read -p "Wrong instance type specified!"
            exit 1
            ;;
esac

echo -n "Enter max price (e.g. 0.4): "
read price
echo -n "How many instances: "
read count
echo -n "region eu-west-1? (e.g. a,b or c): "
read region
aws ec2 request-spot-instances --spot-price $price --instance-count $count --type one-time \
        --launch-specification file:///home/omarkowski/dev/opencloudrender/opencloudrender/instance-configs/afrender_${instance_type}_eu-west-1${region}.json
read -p "Press any key..."
